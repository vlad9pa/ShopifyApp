package com.vlad9pa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopifyappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopifyappApplication.class, args);
	}
}
